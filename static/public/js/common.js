$(window).ready(function() {
  // pre-loader script 
  $(".pre-loader").fadeOut("slow");
  
  // active link 
  for (var i = 0; i < document.links.length; i++) {
   if (document.links[i].href == document.URL) {
   $(document.links[i]).addClass('active');
 }} 
});

// nav-bar
// toggle-btn
$('.toggle-btn').on('click', function() {
 $('.menu').toggleClass('expanded');  
 $('.menu li').toggleClass('hidden');  
 $('.toggle-btn').toggleClass('toggle_btn_on');  
});

// up down scroll
$(window).ready(function() {
 var previousScroll = 0;
 $(window).scroll(function () {
    var currentScroll = $(this).scrollTop();
    if (currentScroll > previousScroll){
        $('body').removeClass("up_scroll");
    }
    else {
        $('body').addClass("up_scroll");
    }
    previousScroll = currentScroll;
 });
}());

// specific_scroll
$(window).scroll(function() {
 if ($(this).scrollTop() > 60){  
     $('body').addClass("specific_scroll");
   }
   else{
     $('body').removeClass("specific_scroll");
   }
});