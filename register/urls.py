from register import views
from django.urls import path
from django.conf.urls import url

urlpatterns = [
     path('',views.login,name='login'),
     path('register/',views.register,name='register'),
     path('view_all/',views.view_all, name = "view_all"),
     url(r'^delete_entry/(?P<user_key>\w+)',views.delete_entry, name = "delete_entry"),
     url(r'^update_entry/(?P<user_key>\w+)',views.update_entry, name = "update_entry"),
     path('login/',views.login, name = "login"),
     url(r'^change_password/(?P<user_key>\w+)',views.change_password, name = "change_password"),
     path('update_entry/',views.update_entry, name = "update_entry"),
     path('logout/',views.logout, name = "logout"),
     path('export/',views.export, name = "export"),
     path('admin/',views.admin, name = "admin"),
     path('upload/',views.upload, name = "upload"),
     # path('simple_upload/',views.simple_upload, name = "simple_upload"),
     # path('search/',views.search, name = "search"),
]