from import_export import resources, fields
from .models import User, Education, Medical
from import_export.widgets import ForeignKeyWidget 

class UserResource(resources.ModelResource):
    # education = fields.Field(column_name='education_fkey', attribute='Education',
                    #    widget=ForeignKeyWidget(Education, 'tenth'))
   
    # education = fields.Field(attribute='edu__tenth')
    class Meta:
        model = User 
        fields = ('user_key','username','edu' ,'email', 'gender','mobile','user_group','country','age','address','pincode','edu__tenth','edu__plustwo','edu__degree','med__height','med__weight')




# class EducationResource(resources.ModelResource):
#     class Meta:
#         model = Education

# class MedicalResource(resources.ModelResource):
#     class Meta:
#         model = Medical