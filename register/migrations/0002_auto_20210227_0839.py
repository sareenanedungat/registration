# Generated by Django 3.1.7 on 2021-02-27 08:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='age',
            field=models.IntegerField(verbose_name=5),
        ),
        migrations.AlterField(
            model_name='user',
            name='pincode',
            field=models.IntegerField(verbose_name=5),
        ),
    ]
