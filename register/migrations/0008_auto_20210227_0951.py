# Generated by Django 3.1.7 on 2021-02-27 09:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0007_auto_20210227_0933'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='mobile',
            field=models.CharField(max_length=50),
        ),
    ]
