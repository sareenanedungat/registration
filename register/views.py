from django.shortcuts import render,redirect
from django.contrib.auth import get_user_model, authenticate
from django.utils.translation import gettext as _
from .models import Education, Medical
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.http import HttpResponse, JsonResponse
from .resources import UserResource
from tablib import Dataset
from django.core.files.storage import FileSystemStorage
import xlwt
import xlrd
from django.db.models import Max
from openpyxl import Workbook
from .validators import MinimumLengthValidator


User = get_user_model()

def register(request):
    
    if request.method == "POST":
        obj = User()
        education = Education()
        medical = Medical()
        validator = MinimumLengthValidator()

        username = request.POST.get("username")
        name =  request.POST.get("name")
        email =  request.POST.get("email")
        gender =  request.POST.get("gender")
        mobile =  request.POST.get("mobile")
        user_group = request.POST.get("user_group")
        country = request.POST.get("country")
        age = request.POST.get("age")
        address = request.POST.get("address")
        pincode = request.POST.get("pincode")
        password = request.POST.get("password")
        tenth = request.POST.get("tenth")
        plustwo = request.POST.get("plustwo")
        degree = request.POST.get("degree")
        height = request.POST.get("height")
        weight = request.POST.get("weight")

        obj.username = username
        obj.name = name
        obj.email = email
        obj.gender = gender
        obj.user_group = user_group
        obj.mobile = mobile
        obj.country = country
        obj.age = age
        obj.address = address
        obj.pincode = pincode
        obj.password = password
        
        validator.validate(password)
        obj.save()
        
        education.education_fkey_id = obj.user_key
        education.tenth = tenth
        education.plustwo = plustwo
        education.degree = degree
        education.save()

        medical.medical_fkey_id = obj.user_key
        medical.height = height
        medical.weight = weight
        medical.save()

        # obj.save()
    return render(request,'register/form.html')

def login(request):
    if request.method == "POST" and 'login' in request.POST:
        
        username = request.POST.get("username")
        password = request.POST.get("password")
        sort_dir = ""
        try:
            obj = User.objects.get(username=username, password=password)
            
        except:
            return HttpResponse("Error Logging in")
        
        request.session['username'] = obj.username
        request.session['user_group'] = obj.user_group
        

        user = request.session['username']

        user_group = request.session['user_group']
        print(user,password)

        user = authenticate(username=username, password=password)

        if User.is_authenticated:
            if obj.user_group == "admin":
                context ={
                'name': obj.username,
                'user_key':obj.user_key,
                'user_group':user_group,
                }
                
                return render(request,'register/admin.html',context)

            elif obj.user_group =="student":
                context ={
                'name': obj.username,
                'user_key':obj.user_key,
                'user_group':user_group,
                }
                
                return render(request,'register/student.html',context)
            
        else:
            return HttpResponse("Thank You")
        
    return render(request,'register/login.html')

def admin(request):
    return render(request,'register/admin.html')

def view_all(request):             
    username = request.session['username']
    print()
    sort_dir = ""
    name = 'registration'
    ob = User.objects.all().exclude(username='admin')
    obj = ob.order_by('-date_joined')
    count = obj.count() 

    
    education = Education.objects.all()
    medical = Medical.objects.all()
    context={
        'values': obj,
        'count':count,
        # 'edu': education,
        # 'med': medical,
    }

    
    if request.method == "GET":
        if request.GET.get('search_by_gender') and request.GET.get('search_by_degree'):
            gender = request.GET.get('search_by_gender')
            degree = request.GET.get('search_by_degree')

            if not gender == 'select' and not degree == 'select':

                obj =  ob.filter(gender=gender,edu__degree = degree)
                count = obj.count()
                context={
                'values': obj,
                'count':count,
                
                }
            elif not gender == 'select' and  degree == 'select':
                obj =  ob.filter(gender=gender)
                count = obj.count()
                context={
                'values': obj,
                'count':count,
                
                }
            elif  gender == 'select' and not degree == 'select':
                obj =  ob.filter(edu__degree=degree)
                count = obj.count()
                context={
                'values': obj,
                'count':count,
                
                }
            context.update({'gender':gender,'degree':degree})
               

    name_sort_dir=request.GET.get('name_sort_dir')
    age_sort_dir =request.GET.get('age_sort_dir')
    pincode_sort_dir =request.GET.get('pincode_sort_dir')
    export_list = request.GET.get('excel_export')
    # print(export_list)
    
    
    if name_sort_dir == 'asc' :
        obj = ob.order_by('name')
        count = obj.count()
        context={
        'values': obj,
        'count': count,
        'name_sort_dir':'asc',
        'export_list':obj,
        }
        export_list = obj
    elif name_sort_dir == 'desc':
        obj = ob.order_by('-name')
        count = obj.count()
        context={
        'values': obj,
        'count': count,
        'name_sort_dir':'desc',
        'export_list':obj,
        }
        export_list = obj

    if age_sort_dir == 'asc':
        obj = ob.order_by('age')
        count = obj.count()
        context={
        'values': obj,
        'count': count,
        'age_sort_dir':'desc',
        'export_list':obj,
        }
        export_list = obj

    elif age_sort_dir == 'desc':
        obj = ob.order_by('-age')
        count = obj.count()
        context={
        'values': obj,
        'count': count,
        'age_sort_dir':'asc',
        'export_list':obj,
        }
        export_list = obj
    
    if pincode_sort_dir == 'asc':
        obj = ob.order_by('pincode')
        count = obj.count()
        context={
        'values': obj,
        'count': count,
        'pincode_sort_dir':'desc',
        'export_list':obj,
        }
        export_list = obj

    elif pincode_sort_dir == 'desc':
        obj = ob.order_by('-pincode')
        count = obj.count()
        context={
        'values': obj,
        'count': count,
        'pincode_sort_dir':'asc',
        'export_list':obj,
        }
        export_list = obj
    
    if request.GET.get('export_data') :
        print(obj,request.GET.get('search_by_gender'))
        
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename="users.xls"'

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Users')
        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['username','name' ,'email', 'gender','mobile','user_group','country','age','address','pincode','tenth','plustwo','degree','height','weight', ]

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)
        
        font_style = xlwt.XFStyle()

        rows = obj.values_list('username','name' ,'email', 'gender','mobile','user_group','country','age','address','pincode','edu__tenth','edu__plustwo','edu__degree','med__height','med__weight',)
        for row in rows:
            row_num += 1
            for col_num in range(len(row)):
                ws.write(row_num, col_num, row[col_num], font_style)

        wb.save(response)
        return response
        
    #import to database
    if request.method == 'POST' and 'hidden_export' in request.POST  :
        
        myfile = request.FILES.get('myfile')
        
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)
        book = xlrd.open_workbook(filename)
        
        # to_save = []
        # list_test=[]
        sheet_number = book.nsheets
        sheet=book.sheet_by_index(0)
        for x in range(1,sheet.nrows):
            user = User()
            med = Medical()
            edu = Education()
            print(obj)

            username = sheet.cell_value(x,0)
           
            
            # max_id = User.objects.all().aggregate(Max('user_key'))
            # existing_user = obj.get(username = username,email = email)
            if obj.filter(username = username).exists():
                continue
            else:
                max_id = obj.aggregate(max_id=Max('user_key'))["max_id"] 
                # max_id = obj.user
                print(max_id)
                user.user_key = max_id+1
                user.username = sheet.cell_value(x,0)
                user.name = sheet.cell_value(x,1)
                user.email =  sheet.cell_value(x,2)
                user.gender =  sheet.cell_value(x,3)
                user.mobile =  sheet.cell_value(x,4)
                user.user_group = sheet.cell_value(x,5)
                user.country = sheet.cell_value(x,6)
                user.age =sheet.cell_value(x,7)
                user.address = sheet.cell_value(x,8)
                user.pincode = sheet.cell_value(x,9)
                user.save()
                # print(sheet.cell_value(x,10))
                edu.education_fkey_id = user.user_key
                edu.tenth = sheet.cell_value(x,10)
                edu.plustwo = sheet.cell_value(x,11)
                edu.degree = sheet.cell_value(x,12)
                edu.save()


                med.medical_fkey_id = user.user_key
                med.height = sheet.cell_value(x,13)
                med.weight = sheet.cell_value(x,14)
                med.save()
             
    return render(request,'register/view_all.html',context)

def delete_entry(request, user_key):
    if id:
        obj = User.objects.get(user_key=user_key)
        obj.delete()
        return redirect('/register/view_all')

    return render(request,'register/view_all.html')

def update_entry(request, user_key):
    obj = User.objects.get(user_key=user_key)
    edu = Education.objects.get(education_fkey=user_key)
    med = Medical.objects.get(medical_fkey=user_key)
    context = {
        'values':obj,
        'edu': edu,
        'med':med,
    }
    if request.method == "POST" and 'update' in request.POST:
    
        obj.username = request.POST.get("username")
        obj.name = request.POST.get("name")
        obj.email = request.POST.get("email")
        obj.gender = request.POST.get("gender")
        obj.user_group = request.POST.get("user_group")
        obj.mobile = request.POST.get("mobile")
        obj.country = request.POST.get("country")
        obj.age = request.POST.get("age")
        obj.address = request.POST.get("address")
        obj.pincode = request.POST.get("pincode")
        obj.password = request.POST.get("password")
        obj.save()
        
        edu.tenth = request.POST.get("tenth")
        edu.plustwo = request.POST.get("plustwo")
        edu.degree = request.POST.get("degree")
        edu.save()


        
        med.height = request.POST.get("height")
        med.weight = request.POST.get("weight")
        med.save()
          

        

        return redirect('/register/view_all')

 

    return render(request,'register/update_entry.html',context)

def change_password(request, user_key):
    if request.method == "POST":
        obj = User.objects.get(user_key=user_key)
        old_password = request.POST.get("old_password")
        new_password = request.POST.get("new_password")
        confirm_password = request.POST.get("confirm_password")

        if obj.password == old_password :
            if new_password == confirm_password:
                validate_password(new_password)
                obj.password = new_password
                obj.save()
            else:
                return HttpResponse("Password and confirm password doesnot match")
        else:
            return HttpResponse("Password doesnot match")

    return render(request,'register/change_password.html')

# def reset_password(request,user_key)   :

def logout(request):
    del request.session['username']
   
    # print(request.session['username'])
    return redirect('login')



def validate_password(password):
    if len(password)<4:
        context ={
            'msg': "Your password must contain at least 4 characters."
        }
        return redirect('register/view_all.html',context)

def upload(request):

    obj = User.objects.all()
    myfile = request.FILES.get('myfile')
    fs = FileSystemStorage()
    filename = fs.save(myfile.name, myfile)
    uploaded_file_url = fs.url(filename)
    book = xlrd.open_workbook(filename)
      
    to_save = {}
    # list_test=[]
    sheet_number = book.nsheets
    sheet=book.sheet_by_index(0)
    for x in range(1,sheet.nrows):
        
        username = sheet.cell_value(x,0)
        user = obj.filter(username=username).exclude(username='admin').exists()
        if not user:
            print(username,"uuu",x)
            
            to_save[x]=username 
        else:
             continue 
                
                
    return JsonResponse(to_save)            
     

def export(request):
    # if obj:
    #     print(obj)    
    user_resource = UserResource()
    dataset = user_resource.export()
    response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="users.xls"'
    return response




    # return render(request, 'register/view_all.html')


    



