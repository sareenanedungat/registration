from django.contrib import admin
from django.contrib.auth import get_user_model
from .models import Education, Medical
from import_export.admin import ImportExportModelAdmin
User = get_user_model()

@admin.register(User)
class UserAdmin(ImportExportModelAdmin):
    pass




# admin.site.register(User)
admin.site.register(Education)
admin.site.register(Medical)
# Register your models here.
