from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
# from phone_field import PhoneField


# class Photo(models.Model):
#     file = models.ImageField(upload_to='profile_picture/',unique=True)
#     description = models.CharField(max_length=255, blank=True)
#     uploaded_at = models.DateTimeField(auto_now_add=True)


class User(AbstractUser):
    name = models.CharField(max_length=100)
    user_key= models.AutoField(primary_key=True)
    user_group = models.CharField(max_length=100)
    gender = models.CharField(max_length=50)
    email = models.EmailField(max_length=100)
    mobile = models.CharField(max_length=50)
    country = models.CharField(max_length=100)
    age = models.IntegerField(null=True)
    address = models.CharField(max_length=150)
    password = models.CharField(max_length=150)
    pincode = models.IntegerField(null=True)
    # photo = models.ForeignKey(Photo,to_field="file", on_delete=models.CASCADE)



class Education(models.Model):
    education_fkey =  models.OneToOneField(User,to_field="user_key", on_delete=models.CASCADE,related_name="edu", )
    tenth = models.CharField(max_length=50)
    plustwo = models.CharField(max_length=50)
    degree = models.CharField(max_length=50)

class Medical(models.Model):
    medical_fkey =  models.OneToOneField(User,to_field="user_key", on_delete=models.CASCADE,related_name="med")
    height = models.IntegerField(null=True)
    weight = models.IntegerField(null=True)
    




